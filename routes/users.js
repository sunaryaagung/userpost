const auth = require("../middleware/auth");
const bcrypt = require("bcryptjs");
const { User, validate } = require("../models/user");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const user = await User.find()
    .populate("post")
    .select("-password");
  res.status(200).json(user);
});

router.get("/me", auth, async (req, res) => {
  const user = await User.findById(req.user._id)
    .populate("post")
    .select("-password");
  res.status(200).json(user);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).send("User is already registered.");

  user = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  });
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  await user.save();
  res.status(201).json(user);
});

router.put("/:id", auth, async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).send("User is already registered.");

  user = await User.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      email: req.body.email
    },
    { new: true }
  );
  if (!user) return res.status(404).send("User not found");

  res.status(200).json(user);
});

router.delete("/:id", auth, async (req, res) => {
  const user = await User.findOneAndDelete(req.params.id);
  if (!user) return res.status(404).send("Invalid User.");
  res.send("Deleted");
});

module.exports = router;
