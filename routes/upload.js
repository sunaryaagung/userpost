//express
const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const { User } = require("../models/user");
// MULTER
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "uploads/");
  },
  filename: function(req, file, cb) {
    console.log(file);
    cb(null, file.originalname);
  }
});

router.post("/", auth, async (req, res, next) => {
  const upload = multer({ storage }).single("filenya");
  upload(req, res, function(err) {
    if (err) {
      res.send(err);
    }
    console.log("File upload to server");
    console.log(req.file);

    //SEND FILE TO CLOUDINARY
    const cloudinary = require("cloudinary").v2;
    cloudinary.config({
      cloud_name: "sunaryaagung",
      api_key: "429485212517494",
      api_secret: "BqFikV6rd-aqtP_M3Wf5i1T8h80"
    });

    const path = req.file.path;
    const uniqueFileName = new Date().toISOString();

    cloudinary.uploader.upload(
      path,
      { public_id: `blog/${uniqueFileName}`, tags: `blog` }, // DIRECTORY AND TAGS ARE OPTIONAL
      async function(err, image) {
        if (err) return res, send(err);
        console.log("File uploaded to cloudinary");
        // REMOVE FILE FROM SERVER
        const fs = require("fs");
        fs.unlinkSync(path);
        // RETURN IMAGE DETAILS
        const user = await User.findByIdAndUpdate(
          req.user._id,
          {
            picture: image.secure_url
          },
          { new: true }
        ).select("-password");
        if (!user) return res.status(404).send("Invalid user");
        res.json(user);
      }
    );
  });
});

module.exports = router;
