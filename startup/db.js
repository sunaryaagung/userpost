const mongoose = require("mongoose");

module.exports = function() {
  const env = process.env.NODE_ENV || "development";

  if (env === "development") {
    require("dotenv").config();
  }

  const configDB = {
    development: process.env.DB_DEV,
    test: process.env.DB_TEST,
    production: process.env.DB_PROD
  };

  const dbConnection = configDB[env];

  mongoose
    .connect(dbConnection, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false
    })
    .then(() => console.log(`Connected to ${dbConnection}`));
};
